# 고전시조 생성
[![code](https://img.shields.io/badge/Code-Python3.7-blue)](https://docs.python.org/3/license.html)
[![data](https://img.shields.io/badge/Data-modern_poem-yellow)](http://www.yunani.or.kr/songgok/simo.htm)

> 소재를 입력하면 고전 시조를 생성하는 프로젝트 

## 📖 Introduction  
주어진 소재로 현대시를 만든 후 고전 시조로 변형함으로써 새롭게 생성된 고전 시조를 보다 쉽게 해석할 수 있고 기존에 누렸던 문화, 경험을 후대가 좀 더 쉽고 풍부하게 접할 수 있도록 하기 위해 진행한 프로젝트입니다. 

## 🏁 Getting Started (Installation)
### Install dependencies
```
- konlpy==0.5.2
- numpy==1.16.1
- pandas==1.1.3
- keras == 2.3.1
- tensorflow == 1.15.0
- beautifulsoup4==4.6.0
```
  
## 💻 How to use
```
python3 run.py 
```

#### 학습된 모델이 존재하지 않는 경우
###### 모델 학습 후 고전시조 생성 

- 현대시 생성 모델 파라메터
    - 데이터 파일 위치 (default: data/total_data.csv)   

- 고전시조 번역 모델 파라메터
    - 사용할 데이터의 최대 길이(default: 60)
    - 임베딩 층 사용 여부 (default: 0)   
    - 입력값의 토큰화 정도 0:글자 1:공백 2:토큰 (default: 0)
    - 출력값의 토큰화 정도 0:글자 1:공백 2:토큰 (default: 0)
    - 입력값 Word2Vec 사용 여부 (default: 0)
    - 출력값 Word2Vec 사용 여부 (default: 0)
    - epoch 수 (default: 300)
    - batch_size (default: 600


#### 학습된 모델이 존재하는 경우 
###### 학습된 모델을 이용하여 고전시조 생성 

- 생성 모델 경로: 경로 입력 
- 번역 모델 경로: 경로 입력 
- 소재: 시 생성을 위한 소재 입력 

- 출력: 생성된 시  

![image](/uploads/57641411b28c36d69036336852e919ca/image.png)


## 👬 Participation Member
- 박성아 : tjdgur265@naver.com
- 박규훤 : svclaw2000@gmail.com