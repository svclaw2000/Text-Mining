import numpy as np
import pickle

sijo = pickle.load(open('../data/preprocessed.pkl', 'rb'))

bind_texts = []

for row in sijo.itertuples():
    temp_input = row[2].split('\n')
    temp_target = row[1].split('\n')
    for _input, _target in zip(temp_input, temp_target):
        if len(_input) > 60:
            continue
        bind_texts.append((_input, '\t%s\n' %_target))

np.random.seed(1234)
np.random.shuffle(bind_texts)
test_size = int(len(bind_texts) * 0.1)
# test_size = 1
train_data, test_data = bind_texts[:-test_size], bind_texts[-test_size:]
input_texts, target_texts = [d[0] for d in train_data], [d[1] for d in train_data]
test_input_texts, test_target_texts = [d[0] for d in test_data], [d[1] for d in test_data]

input_tags = [_input.split() for _input in input_texts]
test_input_tags = [_input.split() for _input in test_input_texts]
output_tags = [target_text.split() for target_text in target_texts]
[tags.insert(0, '\t') for tags in output_tags]
[tags.append('\n') for tags in output_tags]
test_output_tags = [target_text.split() for target_text in test_target_texts]
[tags.insert(0, '\t') for tags in test_output_tags]
[tags.append('\n') for tags in test_output_tags]

output_words = set()
for tags in output_tags:
    for tag in tags:
        if tag not in output_words:
            output_words.add(tag)

for tags in input_tags:
    for tag in tags:
        if tag not in output_words:
            output_words.add(tag)

output_words = sorted(list(output_words))
output_words.insert(0, 'P')
output_words.append('<UNK>')
n_output_words = len(output_words)

max_seq_length = max([len(_in) + len(_out) for _in, _out in zip(input_tags, output_tags)])

w2i_output = {w:i for i, w in enumerate(output_words)}
seq_list = []

for i, (_in, _out) in enumerate(zip(input_tags, output_tags)):
    for j in range(1, len(_out)):
        seq_list.append(_in + _out[:j+1])

n_seq_data = len(seq_list)

seq_data = np.zeros(shape=(n_seq_data, max_seq_length+1), dtype='int32')

for i, seq in enumerate(seq_list):
    seq_data[i, -len(seq):] = np.array([w2i_output[w] for w in seq])

x_seq_data = seq_data[:, :-1]

output_eye = np.eye(n_output_words)
y_seq_data = output_eye[seq_data[:, -1]]

x_train, y_train = x_seq_data, y_seq_data

from keras.models import Sequential
from keras.layers import Embedding, Dense, LSTM

model = Sequential()
model.add(Embedding(n_output_words, 32, input_length=max_seq_length))
model.add(LSTM(256))
model.add(Dense(n_output_words, activation='softmax'))
model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

load_file_name = 'rnn_model_success.h5'
# load_file_name = None
save_file_name = 'rnn_model.h5'

if load_file_name:
    model.load_weights(load_file_name)
else:
    model.fit(x_train, y_train,
              validation_split=0.1,
              batch_size=3500,
              epochs=500)
    model.save(save_file_name)

def sequence_generation(model, tags):
    tags = tags.copy()
    tags.append('\t')
    sentence = ''
    while tags[-1] != '\n' and len(tags) < max_seq_length:
        x_test = np.zeros(shape=(1, max_seq_length), dtype='int32')
        x_test[0, -len(tags):] = [w2i_output[w] if w in w2i_output else w2i_output['<UNK>'] for w in tags]
        result = model.predict_classes(x_test, verbose=0)
        tags.append(output_words[result[0]])
        sentence += ' ' + tags[-1]
    return sentence.replace('\n', '')

start_idx = 30
end_idx = start_idx + 10
for ori, tags, out in zip(input_texts[start_idx:end_idx],
                          input_tags[start_idx:end_idx],
                          target_texts[start_idx:end_idx]):
    print('원본')
    print(ori)
    print('예측')
    print(sequence_generation(model, tags))
    print('정답')
    print(out[1:-1])
    print()

start_idx = 30
end_idx = start_idx + 10
for ori, tags, out in zip(test_input_texts[start_idx:end_idx],
                          test_input_tags[start_idx:end_idx],
                          test_target_texts[start_idx:end_idx]):
    print('원본')
    print(ori)
    print('예측')
    print(sequence_generation(model, tags))
    print('정답')
    print(out[1:-1])
    print()
