from collections import OrderedDict

class Analyzer:
    def __init__(self):
        pass

    def length_distribution(self, data):
        len_dict = {}
        for i in map(len, data):
            if i in len_dict:
                len_dict[i] += 1
            else:
                len_dict[i] = 1
        len_dict = OrderedDict(sorted(len_dict.items()))
        return len_dict