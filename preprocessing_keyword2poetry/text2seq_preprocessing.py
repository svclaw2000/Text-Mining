
# #### 패키지 import

import pandas as pd
import numpy as np

from keras.utils import to_categorical

from keras_preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences


##### data 불러오기 
# 데이터 전처리 
def reprocessing(data):

	data = [s.replace(".",". ") for s in data]
	data = [s.replace("?","? ") for s in data]
	data = [s.replace("!","! ") for s in data]
	data = [s.replace(",",", ") for s in data]
	data = [s.replace("   "," ") for s in data]
	data = [s.replace("  "," ") for s in data]
	data = [s.replace(r"\([^-)]*\)',''s) for s in data]
	data = [s.replace("[^ㄱ-ㅎㅏ-ㅣ가-힣 ]","") for s in data]
	
	return data

# 맞춤법 검사 
def spellchecker(q):
    
    if len(q)<500:
        params = urllib.parse.urlencode({
            "_callback": "",
            "q": q
        })
        
        # 네이버 맞춤법 검사기 사용하여 문법 교정 
        data = urllib.request.urlopen("https://m.search.naver.com/p/csearch/ocontent/spellchecker.nhn?" + params)
        data = data.read().decode("utf-8")[1:-2]
        data = json.loads(data)
        data = data["message"]["result"]["html"]
        data =  BeautifulSoup(data, "html.parser").getText()
        
    else:
        data = ''
    
    return data

data_peonsijo = open("data/peongsijo_modern", encoding = 'utf-8-sig')
data_peonsijo2 = open("data/peongsijo2_modern", encoding = 'utf-8-sig')
data_yeonsijo = open("data/yeonsijo_modern", encoding = 'utf-8-sig')
data_yongbieocheonga = open("data/yongbieocheonga_modern", encoding = 'utf-8-sig')

data_peonsijo = data_peonsijo.read()
data_peonsijo2 = data_peonsijo2.read()
data_yeonsijo = data_yeonsijo.read()
data_yongbieocheonga = data_yongbieocheonga.read()

data = data_peonsijo+'\n'+data_peonsijo2+'\n'+data_yeonsijo+'\n'+data_yongbieocheonga

sample_list = [v for v in data.split('\n') if v]

poem_data = pd.read_csv("data/modern_poem_collection.csv")
poem_data.head()

poem_list = []
poem_list.extend(poem_data["0"].values)
poem_list

total_data_list = sample_list + poem_list

total_data_list = reprocessing(total_data_list)
total_data_list = [spellchecker(data) for data in total_data_list]

total_data_list = [_char for _char in total_data_list if _char]

# 단어 집합 생성 
sequences = list()

t = Tokenizer()
t.fit_on_texts(sample_list)
vocab_size = len(t.word_index)+1

for text in list(sample_list):
    print(text)
    for line in text.split('\n'): # \n을 기준으로 문장 토큰화
        encoded = t.texts_to_sequences([line])[0]  # 텍스트를 숫자 시퀀스로 변경 
        for i in range(1, len(encoded)):
            sequence = encoded[:i+1]
            sequences.append(sequence)

# 인덱스를 단어로 바꾸기 위한 코드 추가  
index_to_word={}
for key, value in t.word_index.items(): # 인덱스를 단어로 바꾸기 위해 index_to_word를 생성
    index_to_word[value] = key

# 문자 길이 맞춰주기 
sequences = pad_sequences(sequences, maxlen=65, padding='pre')  
sequences = np.array(sequences)

# 모델링하기 위해 데이터 저장 
np.save("sequences_save",sequences)


