# 패키지 import 
import pandas as pd
from konlpy.tag import Okt
from konlpy.tag import Kkma 

# 데이터 전처리 
def reprocessing(data):

	data = [s.replace(".",". ") for s in data]
	data = [s.replace("?","? ") for s in data]
	data = [s.replace("!","! ") for s in data]
	data = [s.replace(",",", ") for s in data]
	data = [s.replace("   "," ") for s in data]
	data = [s.replace("  "," ") for s in data]
	data = [s.replace(r"\([^-)]*\)",'',s) for s in data]
	data = [s.replace("[^ㄱ-ㅎㅏ-ㅣ가-힣 ]","") for s in data]
	
	return data

# 맞춤법 검사 
def spellchecker(q):
    
    if len(q)<500:
        params = urllib.parse.urlencode({
            "_callback": "",
            "q": q
        })
        
        # 네이버 맞춤법 검사기 사용하여 문법 교정 
        data = urllib.request.urlopen("https://m.search.naver.com/p/csearch/ocontent/spellchecker.nhn?" + params)
        data = data.read().decode("utf-8")[1:-2]
        data = json.loads(data)
        data = data["message"]["result"]["html"]
        data =  BeautifulSoup(data, "html.parser").getText()
        
    else:
        data = ''
    
    return data

data_peonsijo = open("data/peongsijo_modern", encoding = 'utf-8-sig')
data_peonsijo2 = open("data/peongsijo2_modern", encoding = 'utf-8-sig')
data_yeonsijo = open("data/yeonsijo_modern", encoding = 'utf-8-sig')
data_yongbieocheonga = open("data/yongbieocheonga_modern", encoding = 'utf-8-sig')

data_peonsijo = data_peonsijo.read()
data_peonsijo2 = data_peonsijo2.read()
data_yeonsijo = data_yeonsijo.read()
data_yongbieocheonga = data_yongbieocheonga.read()

data = data_peonsijo+'\n'+data_peonsijo2+'\n'+data_yeonsijo+'\n'+data_yongbieocheonga

sample_list = [v for v in data.split('\n') if v]

poem_data = pd.read_csv("data/modern_poem_collection.csv")
poem_data.head()

poem_list = []
poem_list.extend(poem_data["0"].values)
poem_list

total_data_list = sample_list + poem_list

total_data_list = reprocessing(total_data_list)
total_data_list = [spellchecker(data) for data in total_data_list]

total_data_list = [_char for _char in total_data_list if _char]

## 한글 형태소 분석기를 이용하여 토큰화 
# okt 분석기 
okt = Okt()
okt_tokenized_data = []

for sentence in sijo.modern:
    temp_X = okt.pos(sentence, stem=True) # 토큰화
    okt_tokenized_data.append(temp_X)

# 꼬꼬마 분석기 

# kkma = Kkma()

# kkma_tokenized_data = []

# for sentence in sijo.modern:
#   temp_X = kkma.pos(sentence) # 토큰화
#    kkma_tokenized_data.append(temp_X)


# Mecab 분석기 
#mecab = MeCab.Tagger()

#mecab_tokenized_data = []  # 에러 발생 


# bigram 으로 만들고 word2vec
USE_PREMADE_BIGRAM_MODEL = False

all_unigram_sentences = okt_tokenized_data

if not USE_PREMADE_BIGRAM_MODEL:    
    
    bigram_model = Phrases(all_unigram_sentences) #phrase냐 아니냐를 판단해줌
    
else:
    bigram_model = Phrases.load(bigram_model_filepath)


sum(all_unigram_sentences,[])

# 자주 쓰이는 bigram은 "하노_라" 처럼 묶어짐. 
# https://frhyme.github.io/python-libs/gensim_phrase_model/
for w_l in all_unigram_sentences:
    print(f"raw    sentences: {w_l}")
    bigram_s = bigram_model[w_l]
    print(f"Bigram sentences: {bigram_s}")
    print("--"*20)


# word2vec로 변환 
USE_PREMADE_BIGRAM_SENTENCES = False

bigram_sentences_filepath = 'word2vec/all_sentences_for_word2vec.txt'

if not USE_PREMADE_BIGRAM_SENTENCES:
    
    with open(bigram_sentences_filepath, 'w', encoding='utf-8') as f:
        for unigram_sentence in all_unigram_sentences:
            bigram_sentence = bigram_model[unigram_sentence]
            f.write(' '.join(bigram_sentence) + '\n')
else:
    assert path.exists(bigram_sentences_filepath)


USE_PREMADE_WORD2VEC = False

all2vec_filepath = 'word2vec/all_word2vec_model'

if not USE_PREMADE_WORD2VEC:
    
    modern_for_word2vec = LineSentence(bigram_sentences_filepath)

    all2vec = Word2Vec(modern_for_word2vec, size=20, window=3, min_count=1, sg=1)
    # sg=0 cbow 1=Skip-Gram Model
    # size =100차원으로 가져옴 / 보통 20~100 정도
    # window = 5 앞 5개, 뒤 5개 단어를 보겠다는 뜻
    # window size 작을수록 문법적인 의미가 너무 중요해짐, 클수록 주제 지향적으로 문맥적인 정보를 많이 담게 됨    
    # min_count: x번 이상 등장한 단어만 

all2vec.init_sims()

# 확인 
for i in modern_for_word2vec:
    print(i)