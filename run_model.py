import pandas as pd
import numpy as np

from keras_preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
import datetime

from tensorflow.keras.utils import to_categorical
import re
from keras.preprocessing import sequence
from keras.models import Sequential

from keras.layers import Dense, Dropout, Embedding, LSTM, Bidirectional, GRU
from keras.layers import CuDNNLSTM
from tensorflow.keras.callbacks import EarlyStopping


# 모델이 메모리로 인해 안돌아갈 때 사용
# MEMORY_LIMIT = 1024
# gpus = tf.config.experimental.list_physical_devices('GPU')
# if gpus:
#     try:
#         tf.config.experimental.set_virtual_device_configuration(gpus[0], [tf.config.experimental.VirtualDeviceConfiguration(memory_limit=MEMORY_LIMIT)])
#     except RuntimeError as e:
#         print(e)

class Generate_poem:

    def __init__(self):

        self.t = Tokenizer()
        self.X = None
        self.y = None
        self.max_len = 0
        self.vocab_size = 0

    def data_preprocess(self, file):

        data = pd.read_csv(file, encoding="utf-8-sig").modern.values

        stop_words = '이 저 그 내 다 을 를 가 듯 의 에 좀 다'

        stop_words = stop_words.split(' ')

        result = []

        for line in data:

            word_tokens = line.split(' ')

            if line not in stop_words:
                result.append(line)

        self.t.fit_on_texts(result)
        vocab_size = len(self.t.word_index) + 1
        sequences = list()

        for line in result:

            encoded = self.t.texts_to_sequences([line])[0]  # 각 샘플에 대한 정수 인코딩

            for i in range(1, len(encoded)):
                sequence = encoded[:i + 1]
                sequences.append(sequence)

        max_len = max(len(l) for l in sequences)
        sequences = pad_sequences(sequences, maxlen=max_len, padding='pre')
        sequences = np.array(sequences)

        X = sequences[:, :-1]
        y = sequences[:, -1]
        y = to_categorical(y, num_classes=vocab_size)

        self.X = X
        self.y = y
        self.vocab_size = vocab_size
        self.max_len = max_len

    def create_model(self, epochs=250, batch_size=1000, savepath='generate_model.h5', loadpath=None):

        embedding_dim = 100

        model = Sequential()

        model.add(Embedding(self.vocab_size, embedding_dim, input_length=self.max_len - 1))  # 레이블을 분리하였으므로 이제 X의 길이는 5
        model.add(Bidirectional(CuDNNLSTM(64, return_sequences=True)))
        model.add(Dropout(0.3))
        model.add(Bidirectional(CuDNNLSTM(64, return_sequences=False)))
        model.add(Dense(self.vocab_size, activation='softmax'))

        if loadpath:
            model.load_weights(loadpath)
        else:
            model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
            model.fit(self.X, self.y, epochs=epochs, batch_size=batch_size, validation_split=0.1)
            model.save(savepath)
            print('%s에 생성 모델이 저장되었습니다.' %savepath)

        self.model = model

    def sentence_generation(self, initial_word=None, next_word_len=0):  # 모델, 토크나이저, 현재 단어, 반복할 횟수

        init_word = initial_word  # 처음 들어온 단어도 마지막에 같이 출력하기위해 저장

        sentence = ''

        for _ in range(next_word_len):  # n번 반복

            encoded = self.t.texts_to_sequences([initial_word])[0]  # 현재 단어에 대한 정수 인코딩
            encoded = pad_sequences([encoded], maxlen=self.max_len - 1, padding='pre')  # 데이터에 대한 패딩
            result = self.model.predict_classes(encoded, verbose=0)

            # 입력한 X(현재 단어)에 대해서 Y를 예측하고 Y(예측한 단어)를 result에 저장.

            for word, index in self.t.word_index.items():

                if index == result:  # 만약 예측한 단어와 인덱스와 동일한 단어가 있다면

                    break  # 해당 단어가 예측 단어이므로 break

            initial_word = initial_word + ' ' + word  # 현재 단어 + ' ' + 예측 단어를 현재 단어로 변경
            sentence = init_word + ' ' + word  # 예측 단어를 문장에 저장

        # for문이므로 이 행동을 다시 반복

        self.sentence = initial_word + sentence

        return self.sentence
