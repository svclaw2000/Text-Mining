import numpy as np
import pandas as pd

from keras.layers import Embedding, Dense, SimpleRNN
from keras.models import Sequential
from keras.layers import Dense,Dropout,SimpleRNN,LSTM
from tensorflow.keras.callbacks import EarlyStopping
from keras.utils import to_categorical
from keras_preprocessing.text import Tokenizer

# 전처리 된 데이터 불러오기 
np_load_old = np.load

old = np.load
np.load = lambda *a,**k: old(*a,allow_pickle=True)
sequences = np.load("sequences_save.npy")

vocab_size = 2567  # 전처리 때 확인한 vocab size

X = sequences[:, :-1]
y = sequences[:, -1]

y = to_categorical(y, num_classes=vocab_size)


# 모델 생성 
model = Sequential()
model.add(Embedding(vocab_size, 10, input_length=64))

model.add(LSTM(32))
model.add(Dense(vocab_size, activation='softmax'))
model.compile(loss='categorical_crossentropy', optimizer='adam',
             metrics=['accuracy'])

early_stop = EarlyStopping(monitor='loss',patience=1,verbose=1)
model.fit(X, y, epochs=50, verbose=2, callbacks=[early_stop])


def sentence_generation(model, t, current_word, n):
    # 모델, 토크나이저, 현재 단어, 반복 횟수
    init_word = current_word
    # 처음 들어온 단어도 마지막에 출력하기 위해 저장
    sentence = ''
    for _ in range(n):
        encoded = t.texts_to_sequences([current_word])[0]
        # 현재 단어에 대한 정수 인코딩
        encoded = pad_sequences([encoded], maxlen=64, padding='pre')
        # 데이터에 대한 패딩
        result = model.predict_classes(encoded, verbose=0)
        # 입력한 X(현재 단어)에 대해서 Y를 입력하고
        # Y(예측한 단어)를 result에 저장.
        
        for word, index in t.word_index.items():
            if index == result:
                break
        current_word = current_word + ' ' + word
        # 현재 단어 + ' ' + 예측 단어를 현재 단어로 변경
        sentence = sentence + ' ' + word
        
    # 전체 반복
    sentence = init_word + sentence
    
    return sentence

sijo.modern = pd.read_pickle("sijo_modern.pkl")

t = Tokenizer()
t.fit_on_texts(sijo.modern)

sentence = sentence_generation(model, t, '사랑', 10 ) 