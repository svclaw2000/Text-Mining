import numpy as np
import pickle

sijo = pickle.load(open('../data/preprocessed.pkl', 'rb'))

bind_texts = []

for row in sijo.itertuples():
    temp_input = row[2].split('\n')
    temp_target = row[1].split('\n')
    for _input, _target in zip(temp_input, temp_target):
        if len(_input) > 60:
            continue
        bind_texts.append((_input, '\t%s\n' %_target))

np.random.seed(1234)
np.random.shuffle(bind_texts)
test_size = int(len(bind_texts) * 0.1)
train_data, test_data = bind_texts[:-test_size], bind_texts[-test_size:]
input_texts, target_texts = [d[0] for d in train_data], [d[1] for d in train_data]
test_input_texts, test_target_texts = [d[0] for d in test_data], [d[1] for d in test_data]

from utils.komoran_extractor import Extractor

extractor = Extractor()
input_tags = [[tag.split('/')[0] for tag in _input] for _input in extractor.extract(input_texts)]

from gensim.models import word2vec

ko_model = word2vec.Word2Vec.load('ko.bin')

# target_tags = [[c for c in target_text] for target_text in target_texts]
target_tags = []

for target_text in target_texts:
    tags = target_text.split()
    tags.insert(0, '\t')
    tags.append('\n')
    target_tags.append(tags)

tr_model = word2vec.Word2Vec(
    target_tags,
    workers=4,
    size=200,
    window=5,
    sample=1e-3
)

latent_dim = 256

num_encoder_tokens = 200
num_decoder_tokens = 200
max_encoder_seq_length = max([len(txt) for txt in input_tags])
max_decoder_seq_length = max([len(txt) for txt in target_tags])

encoder_input_data = np.zeros(shape=(len(input_texts), max_encoder_seq_length, num_encoder_tokens), dtype='float32')
decoder_input_data = np.zeros(shape=(len(target_texts), max_decoder_seq_length, num_decoder_tokens), dtype='float32')
decoder_target_data = np.zeros(shape=(len(target_texts), max_decoder_seq_length,
                                      num_decoder_tokens), dtype='float32')

for i, (input_tag, target_tag) in enumerate(zip(input_tags, target_tags)):
    for j, w in enumerate(input_tag):
        if w in ko_model.wv:
            encoder_input_data[i, j] = ko_model.wv[w]
    for j, w in enumerate(target_tag):
        if w in tr_model.wv:
            decoder_input_data[i, j] = tr_model.wv[w]
            if j > 0:
                decoder_target_data[i, j-1] = tr_model.wv[w]

from keras.models import Model, load_model
from keras.layers import Input, LSTM, Dense, BatchNormalization, Dropout, Embedding, Reshape
from keras.callbacks import EarlyStopping

# a part of encoder

num_embedding = 128

encoder_inputs = Input(shape=(None, num_encoder_tokens), name='encoder_input')
encoder = LSTM(latent_dim, return_sequences=True, return_state=True, name='encoder')
encoder_outputs, state_h, state_c = encoder(encoder_inputs)
encoder_states = [state_h, state_c]

# a part of decoder
decoder_inputs = Input(shape=(None, num_decoder_tokens), name='decoder_input')
decoder_lstm = LSTM(latent_dim, return_sequences=True, return_state=True, name='decoder')
decoder_outputs, _, _ = decoder_lstm(decoder_inputs, initial_state=encoder_states)
dropout = Dropout(0.2)
batchNorm = BatchNormalization()
decoder_dense = Dense(num_decoder_tokens, activation='softmax')
decoder_outputs = decoder_dense(batchNorm(dropout(decoder_outputs)))

# a model to train
model = Model([encoder_inputs, decoder_inputs], decoder_outputs)

batch_size=600
epochs=200
optimizer='adam'
loss='mae'
# load_model_path='s2s_success.h5'
load_model_path=None
save_model_path='s2s.h5'

if load_model_path is not None:
    model = model.load_weights(load_model_path)
else:
    model.compile(optimizer, loss)
    model.fit([encoder_input_data, decoder_input_data], decoder_target_data,
              batch_size=batch_size,
              epochs=epochs,
              validation_split=0.2,
              callbacks=[EarlyStopping(monitor='val_loss', patience=5, verbose=0, mode='auto')]
              )
    model.save(save_model_path)

# encoder model to decode
encoder_model = Model(encoder_inputs, encoder_states)

# decoder model to decode
decoder_state_input_h = Input(shape=(latent_dim,))
decoder_state_input_c = Input(shape=(latent_dim,))
decoder_states_inputs = [decoder_state_input_h, decoder_state_input_c]
decoder_outputs, state_h, state_c = decoder_lstm(decoder_inputs, initial_state=decoder_states_inputs)
decoder_states = [state_h, state_c]
decoder_outputs = decoder_dense(decoder_outputs)
decoder_model = Model(
    [decoder_inputs] + decoder_states_inputs,
    [decoder_outputs] + decoder_states)

def decode_sequence(input_seq):
    # Encode the input as state vectors. [state_h, state_c]
    states_value = encoder_model.predict(input_seq)

    # Generate empty target sequence of length 1.
    target_seq = np.zeros((1, 1, num_decoder_tokens))
    # Populate the first character of target sequence with the start character.
    target_seq[0, 0] = tr_model.wv['\t']

    # 점점 디코드된 문자열을 추가해나감.
    stop_condition = False
    decoded_sentence = ''
    while not stop_condition:
        output_tokens, h, c = decoder_model.predict([target_seq] + states_value)

        # Sample a token
        sampled_token = tr_model.wv.most_similar(positive=[output_tokens[0, -1, :]])
        sampled_char = sampled_token[0][0]
        decoded_sentence += sampled_char

        # Exit condition: either hit max length
        # or find stop character.
        if (sampled_char == '\n' or
                len(decoded_sentence) > max_decoder_seq_length):
            stop_condition = True

        # Update the target sequence (of length 1).
        target_seq = np.zeros((1, 1, num_decoder_tokens))
        target_seq[0, 0] = output_tokens[0, -1, :]

        # Update states
        states_value = [h, c]

    return decoded_sentence

def translate(input_seq):
    # 입력을 모델에 주입할 수 있도록 벡터화한다.
    input_vec = np.zeros(shape=(len(input_seq), max_encoder_seq_length, num_encoder_tokens), dtype='float32')
    input_tags = [[tag.split('/')[0] for tag in _input] for _input in extractor.extract(input_seq)]
    for i, tags in enumerate(input_tags):
        for j, w in enumerate(tags):
            if w in ko_model.wv:
                input_vec[i, j] = ko_model.wv[w]

    decoded_sequences = []
    for seq_idx in range(input_vec.shape[0]):
        decoded_sequences.append(decode_sequence(input_vec[seq_idx: seq_idx+1]))

    return decoded_sequences

start_idx = 30
end_idx = start_idx + 10

for _input, _target, _pred in zip(input_texts[start_idx:end_idx], target_texts[start_idx:end_idx], translate(input_texts[start_idx:end_idx])):
    print(_input)
    print(_target[1:-1])
    print(_pred)
    print()

start_idx = 10
end_idx = start_idx + 10

for _input, _target, _pred in zip(test_input_texts[start_idx:end_idx], test_target_texts[start_idx:end_idx], translate(test_input_texts[start_idx:end_idx])):
    print(_input)
    print(_target[1:-1])
    print(_pred)
    print()