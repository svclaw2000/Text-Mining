# 패키지 import 
from urllib.request import urlopen
from bs4 import BeautifulSoup
import requests
import pandas as pd

import re
import json


# 현대 시 모음 url 
URL = "http://www.yunani.or.kr/songgok/simo.htm"

# parsing 하기 
base = requests.get(URL)
base.encoding ='cp949'
soup = BeautifulSoup(base.text, "html.parser")


# 데이터 추출
link_total = []

for link in soup.select("p>a"):
    link_total.append(link.attrs['href'])

# 시를 담을 빈 list 생성 
poem_content = []

# 현대시 크롤링 
for poem_link in link_total:
    
    try: 
        base = requests.get(poem_link)
        base.encoding = "cp949"
        
        soup = BeautifulSoup(base.text, "html.parser")

        poem1 = soup.select("body > table:nth-child(1)>tr")[1].text.replace("\n"," ").replace("\xa0","").strip()
        poem2 = re.split(r'<|-',soup.select("body > table:nth-child(1)>tr")[2].text)[0].
            replace("\n"," ").replace("\xa0","").strip()

        print(poem_link)
        print(poem1+poem2)
        
        poem_content.append((poem1+" " +poem2))
        
        
    except:
        print("Except", poem_link)  # 찾을 수 없는 link로 인해 에러 발생함을 확인 
 

# 문법 교정 함수 생성 
def spellchecker(q):

    if len(q) < 500:  # 문자열이 너무 긴 경우는 제외 

        params = urllib.parse.urlencode({
            "_callback": "",
            "q": q
        })

        # 네이버 맞춤법 검사기 사용하여 문법 교정 
        data = urllib.request.urlopen("https://m.search.naver.com/p/csearch/ocontent/spellchecker.nhn?" + params)
        data = data.read().decode("utf-8")[1:-2]
        data = json.loads(data)
        data = data["message"]["result"]["html"]
        data = soup = BeautifulSoup(data, "html.parser").getText()

    else:
        data = ''
    
    return data

# 잘못된 글자 교정 
def clean_sentence(s, str_type=None):
    s = re.sub(r'\([^)]*\)','',s)  # 한자 제거
    s = s.replace("…"," ").replace("                          "," ").replace("Ⅰ","").replace("ll","").
            replace("              "," ").replace("  "," ").replace("―","").replace("---","") 
    s = "".join(re.compile("[^0-9]").findall(s))  # 숫자 제거
    
    # 문법이 잘못된 경우 교정된 시로 대체
    if len(s) <=1000:
        
        s = spellchecker(s)     

    return s

total_poem = [clean_sentence(poem) for poem in poem_content]

print(len(total_poem))  # 교정된 poem 개수: 218
print(len(poem_content))  # 교정되기 전 poem 개수: 218

# 빈 값 없음 확인 
count = 0
for poem in total_poem:
    if poem == '':
        count+=1

print(count)

# 수집된 데이터 저장 
pd.DataFrame(total_poem).to_pickle("total_poem.pkl")




