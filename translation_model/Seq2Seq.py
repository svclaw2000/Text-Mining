from datetime import datetime
import pickle
import numpy as np
from utils.komoran_extractor import Extractor
from keras.models import Model
from keras.layers import Input, LSTM, Dense, BatchNormalization, Dropout, Embedding
from keras.callbacks import EarlyStopping
from gensim.models import Word2Vec
import os


class S2S_Model:
    def __init__(self, load_path=None, max_length=60, embedding=False,
                 input_tokenize=0, output_tokenize=0, input_pos=("/N", "/V", "/M", "/IC", "/XR"),
                 input_gensim=0, output_gensim=0, epochs=300, batch_size=600):
        self.save_path = 'model_%s.h5' % datetime.now().strftime('%y%m%d_%H%M%S')
        self.config = {'load_path': load_path,
                       'max_length': max_length,
                       'embedding': embedding,
                       'input_tokenize': input_tokenize,
                       'output_tokenize': output_tokenize,
                       'input_pos': input_pos,
                       'input_gensim': input_gensim,
                       'output_gensim': output_gensim,
                       'epochs': epochs,
                       'batch_size': batch_size}

        if load_path:
            self.config = pickle.load(open(load_path + '.pkl', 'rb'))

        self.max_length = self.config['max_length']
        self.embedding = self.config['embedding']
        self.input_tokenize = self.config['input_tokenize']
        self.output_tokenize = self.config['output_tokenize']
        self.input_pos = self.config['input_pos']
        self.input_gensim = self.config['input_gensim']
        self.output_gensim = self.config['output_gensim']

        if input_gensim and embedding:
            print('Gensim(Word2Vec)과 Embedding 옵션은 동시에 사용할 수 없습니다.')
            exit()

        self.extractor = Extractor()

        try:
            self.raw_data = self.load_data()
        except:
            print('데이터를 로드하는 중에 에러가 발생했습니다. 전처리 코드가 동작했는지 확인해주세요.')
            exit()

        self.tokens = self.tokenize_data()
        if input_gensim == 1:
            self.input_w2v = Word2Vec.load('data/ko.bin').wv
        elif input_gensim == 2:
            self.input_w2v = Word2Vec(
                self.tokens[0][0],
                workers=os.cpu_count(),
                size=200,
                window=5,
                sample=1e-3
            ).wv

        if output_gensim == 1:
            self.output_w2v = Word2Vec(
                self.tokens[0][1],
                workers=os.cpu_count(),
                size=200,
                window=5,
                sample=1e-3
            ).wv

        self.num_encoder_tokens = None
        self.max_encoder_seq_length = None
        self.input_tokens = None
        self.input_token_index = None

        self.num_decoder_tokens = None
        self.max_decoder_seq_length = None
        self.output_tokens = None
        self.output_token_index = None

        self.data = self.make_data()

        self.models = self.build_model()

        if load_path:
            self.load(load_path)
        else:
            self.train(epochs=self.config['epochs'], batch_size=self.config['batch_size'])

    def load_data(self, data_path='data/preprocessed.pkl', random_seed=123456, test_ratio=0.1):
        sijo = pickle.load(open(data_path, 'rb'))
        bind_texts = []
        for row in sijo.itertuples():
            temp_input = row[2].split('\n')
            temp_target = row[1].split('\n')
            for _input, _target in zip(temp_input, temp_target):
                if len(_input) > self.max_length:
                    continue
                bind_texts.append((_input, '\t%s\n' % _target))

        np.random.seed(random_seed)
        np.random.shuffle(bind_texts)
        test_size = int(len(bind_texts) * test_ratio)
        train_data, test_data = bind_texts[:-test_size], bind_texts[-test_size:]

        input_texts, target_texts = [d[0] for d in train_data], [d[1] for d in train_data]
        test_input_texts, test_target_texts = [d[0] for d in test_data], [d[1] for d in test_data]

        return (input_texts, target_texts), (test_input_texts, test_target_texts)

    def tokenize_data(self):
        if self.input_tokenize == 0:
            train_x = [[c for c in s] for s in self.raw_data[0][0]]
            test_x = [[c for c in s] for s in self.raw_data[1][0]]
        elif self.input_tokenize == 1:
            train_x = [s.split() for s in self.raw_data[0][0]]
            for x in train_x:
                x.insert(0, '\t')
                x.append('\n')
            test_x = [s.split() for s in self.raw_data[1][0]]
            for x in test_x:
                x.insert(0, '\t')
                x.append('\n')
        elif self.input_tokenize == 2:
            train_x = self.extractor.extract(self.raw_data[0][0], self.input_pos)
            test_x = self.extractor.extract(self.raw_data[1][0], self.input_pos)
        else:
            print('input_tokenize 값은 0(글자), 1(공백), 2(토큰) 중 하나로 입력해주세요.')
            exit()

        if self.output_tokenize == 0:
            train_y = [[c for c in s] for s in self.raw_data[0][1]]
            test_y = [[c for c in s] for s in self.raw_data[1][1]]
        elif self.output_tokenize == 1:
            train_y = [s.split() for s in self.raw_data[0][1]]
            for y in train_y:
                y.insert(0, '\t')
                y.append('\n')
            test_y = [s.split() for s in self.raw_data[1][1]]
            for y in train_y:
                y.insert(0, '\t')
                y.append('\n')
        else:
            print('output_tokenize 값은 0(글자), 1(공백) 중 하나로 입력해주세요.')
            exit()

        return (train_x, train_y), (test_x, test_y)

    def make_data(self):
        if self.input_gensim == 1 or self.input_gensim == 2:
            self.num_encoder_tokens = 200
            self.max_encoder_seq_length = max([len(s) for s in self.tokens[0][0]])
            encoder_input_data = np.zeros(shape=(len(self.tokens[0][0]), self.max_encoder_seq_length,
                                                 self.num_encoder_tokens), dtype='float32')
            for i, input_tags in enumerate(self.tokens[0][0]):
                for j, w in enumerate(input_tags):
                    if w in self.input_w2v:
                        encoder_input_data[i, j] = self.input_w2v[w]
        else:
            input_tokens = set()
            for _input in self.tokens[0][0]:
                for t in _input:
                    if t not in input_tokens:
                        input_tokens.add(t)
            input_tokens = sorted(list(input_tokens))
            input_tokens.append('<PAD>')
            self.input_tokens = input_tokens
            self.num_encoder_tokens = len(input_tokens)
            self.max_encoder_seq_length = max([len(s) for s in self.tokens[0][0]])
            self.input_token_index = {c: i for i, c in enumerate(self.input_tokens)}

            if self.embedding:
                encoder_input_data = np.zeros(shape=(len(self.tokens[0][0]), self.max_encoder_seq_length),
                                              dtype='float32')
                for i, input_tags in enumerate(self.tokens[0][0]):
                    for j, w in enumerate(input_tags):
                        encoder_input_data[i, j] = self.input_token_index[w]
            else:
                encoder_input_data = np.zeros(shape=(len(self.tokens[0][0]), self.max_encoder_seq_length,
                                                     self.num_encoder_tokens), dtype='float32')
                for i, input_tags in enumerate(self.tokens[0][0]):
                    for j, w in enumerate(input_tags):
                        encoder_input_data[i, j, self.input_token_index[w]] = 1.

        if self.output_gensim == 1:
            self.num_decoder_tokens = 200
            self.max_decoder_seq_length = max([len(s) for s in self.tokens[0][1]])
            decoder_input_data = np.zeros(shape=(len(self.tokens[0][1]), self.max_decoder_seq_length,
                                                 self.num_decoder_tokens), dtype='float32')
            decoder_output_data = np.zeros(shape=(len(self.tokens[0][1]), self.max_decoder_seq_length,
                                                  self.num_decoder_tokens), dtype='float32')
            for i, output_tags in enumerate(self.tokens[0][1]):
                for j, w in enumerate(output_tags):
                    if w in self.output_w2v:
                        decoder_input_data[i, j] = self.input_w2v[w]
                        if j > 0:
                            decoder_output_data[i, j - 1] = self.input_w2v[w]
        else:
            output_tokens = set()
            for _output in self.tokens[0][1]:
                for t in _output:
                    if t not in output_tokens:
                        output_tokens.add(t)
            output_tokens = sorted(list(output_tokens))
            output_tokens.insert(0, '<PAD>')
            output_tokens.append('<UNK>')
            self.output_tokens = output_tokens
            self.num_decoder_tokens = len(output_tokens)
            self.max_decoder_seq_length = max([len(s) for s in self.tokens[0][1]])
            self.output_token_index = {c: i for i, c in enumerate(self.output_tokens)}

            if self.embedding:
                decoder_input_data = np.zeros(shape=(len(self.tokens[0][1]), self.max_decoder_seq_length),
                                              dtype='float32')
                decoder_output_data = np.zeros(shape=(len(self.tokens[0][1]), self.max_decoder_seq_length,
                                                      self.num_decoder_tokens), dtype='float32')

                for i, output_tags in enumerate(self.tokens[0][1]):
                    for j, w in enumerate(output_tags):
                        decoder_input_data[i, j] = self.output_token_index[w]
                        if j > 0:
                            decoder_output_data[i, j - 1, self.output_token_index[w]] = 1.
            else:
                decoder_input_data = np.zeros(shape=(len(self.tokens[0][1]), self.max_decoder_seq_length,
                                                     self.num_decoder_tokens), dtype='float32')
                decoder_output_data = np.zeros(shape=(len(self.tokens[0][1]), self.max_decoder_seq_length,
                                                      self.num_decoder_tokens), dtype='float32')

                for i, output_tags in enumerate(self.tokens[0][1]):
                    for j, w in enumerate(output_tags):
                        decoder_input_data[i, j, self.output_token_index[w]] = 1.
                        if j > 0:
                            decoder_output_data[i, j - 1, self.output_token_index[w]] = 1.

        return encoder_input_data, decoder_input_data, decoder_output_data

    def fit_data(self, sentences):
        if self.input_tokenize == 0:
            tokens = [[c for c in s] for s in sentences]
        elif self.input_tokenize == 1:
            tokens = [s.split() for s in sentences]
            for x in tokens:
                x.insert(0, '\t')
                x.append('\n')
        elif self.input_tokenize == 2:
            tokens = self.extractor.extract(sentences, self.input_pos)

        if self.input_gensim == 1 or self.input_gensim == 2:
            encoder_input_data = np.zeros(shape=(len(tokens), self.max_encoder_seq_length,
                                                 self.num_encoder_tokens), dtype='float32')
            for i, input_tags in enumerate(tokens):
                for j, w in enumerate(input_tags):
                    if w in self.input_w2v:
                        encoder_input_data[i, j] = self.input_w2v[w]
        elif self.embedding:
            encoder_input_data = np.zeros(shape=(len(tokens), self.max_encoder_seq_length),
                                          dtype='float32')
            for i, input_tags in enumerate(tokens):
                for j, w in enumerate(input_tags):
                    encoder_input_data[i, j] = self.input_token_index[w] if w in self.input_token_index else self.input_token_index['<PAD>']
        else:
            encoder_input_data = np.zeros(shape=(len(tokens), self.max_encoder_seq_length,
                                                 self.num_encoder_tokens), dtype='float32')
            for i, input_tags in enumerate(tokens):
                for j, w in enumerate(input_tags):
                    encoder_input_data[i, j, self.input_token_index[w] if w in self.input_token_index else self.input_token_index['<PAD>']] = 1.

        return encoder_input_data

    def train(self, optimizer='adam', loss='categorical_crossentropy', batch_size=600, epochs=300,
              valid_ratio=0.1, early_stopping=False):
        model = self.models[0]
        model.compile(optimizer, loss)
        model.fit([self.data[0], self.data[1]], self.data[2],
                  batch_size=batch_size,
                  epochs=epochs,
                  validation_split=valid_ratio,
                  callbacks=[EarlyStopping(monitor='val_loss', patience=5, verbose=0, mode='auto')]
                  if early_stopping else [])
        model.save(self.save_path)
        pickle.dump(self.config, open(self.save_path + '.pkl', 'wb'))
        print('%s에 번역 모델이 저장되었습니다.' %(self.save_path))

    def load(self, load_path):
        try:
            model = self.models[0]
            model.load_weights(load_path)
        except:
            print('모델 로드 중 오류 발생. 모델과 데이터를 확인해주세요.')
            exit()

    def build_model(self, latent_dim=256, num_embedding=128):
        if self.embedding:
            encoder_inputs = Input(shape=(None,), name='encoder_input')
            _encoder_inputs = Embedding(self.num_encoder_tokens, num_embedding)(encoder_inputs)

            decoder_inputs = Input(shape=(None,), name='decoder_input')
            _decoder_inputs = Embedding(self.num_decoder_tokens, num_embedding)(decoder_inputs)
        else:
            _encoder_inputs = encoder_inputs = Input(shape=(None, self.num_encoder_tokens), name='encoder_input')
            _decoder_inputs = decoder_inputs = Input(shape=(None, self.num_decoder_tokens), name='decoder_input')
        encoder = LSTM(latent_dim, return_sequences=True, return_state=True, name='encoder')
        encoder_outputs, state_h, state_c = encoder(_encoder_inputs)
        encoder_states = [state_h, state_c]

        decoder_lstm = LSTM(latent_dim, return_sequences=True, return_state=True, name='decoder')
        decoder_outputs, _, _ = decoder_lstm(_decoder_inputs, initial_state=encoder_states)
        dropout = Dropout(0.2)
        batchNorm = BatchNormalization()
        decoder_dense = Dense(self.num_decoder_tokens, activation='softmax')
        decoder_outputs = decoder_dense(batchNorm(dropout(decoder_outputs)))

        model = Model([encoder_inputs, decoder_inputs], decoder_outputs)

        encoder_model = Model(encoder_inputs, encoder_states)

        decoder_state_input_h = Input(shape=(latent_dim,))
        decoder_state_input_c = Input(shape=(latent_dim,))
        decoder_states_inputs = [decoder_state_input_h, decoder_state_input_c]
        decoder_outputs, state_h, state_c = decoder_lstm(_decoder_inputs, initial_state=decoder_states_inputs)
        decoder_states = [state_h, state_c]
        decoder_outputs = decoder_dense(decoder_outputs)
        decoder_model = Model(
            [decoder_inputs] + decoder_states_inputs,
            [decoder_outputs] + decoder_states)

        return model, encoder_model, decoder_model

    def decode_sequence(self, input_seq):
        encoder_model, decoder_model = self.models[1], self.models[2]

        if self.embedding:
            states_value = encoder_model.predict(input_seq)

            output_seq = np.zeros((1, 1))
            output_seq[0, 0] = self.output_token_index['\t']

            stop_condition = False
            decoded_sentence = ''
            while not stop_condition:
                output_tokens, h, c = decoder_model.predict([output_seq] + states_value)

                # Sample a token
                sampled_token_index = np.argmax(output_tokens[0, -1, :])
                sampled_char = self.output_tokens[sampled_token_index]
                decoded_sentence += ' ' + sampled_char

                # Exit condition: either hit max length
                # or find stop character.
                if (sampled_char == '\n' or
                        len(decoded_sentence) > self.max_decoder_seq_length):
                    stop_condition = True

                # Update the target sequence (of length 1).
                target_seq = np.zeros((1, 1))
                target_seq[0, 0] = sampled_token_index

                # Update states
                states_value = [h, c]

            return decoded_sentence
        elif self.output_gensim == 1:
            states_value = encoder_model.predict(input_seq)

            target_seq = np.zeros((1, 1, self.num_decoder_tokens))
            target_seq[0, 0] = self.output_w2v['\t']

            stop_condition = False
            decoded_sentence = ''
            while not stop_condition:
                output_tokens, h, c = decoder_model.predict([target_seq] + states_value)

                sampled_token = self.output_w2v.most_similar(positive=[output_tokens[0, -1, :]])
                sampled_char = sampled_token[0][0]
                decoded_sentence += sampled_char

                if (sampled_char == '\n' or
                        len(decoded_sentence) > self.max_decoder_seq_length):
                    stop_condition = True

                target_seq = np.zeros((1, 1, self.num_decoder_tokens))
                target_seq[0, 0] = output_tokens[0, -1, :]

                states_value = [h, c]

            return decoded_sentence
        else:
            states_value = encoder_model.predict(input_seq)

            target_seq = np.zeros((1, 1, self.num_decoder_tokens))
            target_seq[0, 0, self.output_token_index['\t']] = 1.

            stop_condition = False
            decoded_sentence = ''
            while not stop_condition:
                output_tokens, h, c = decoder_model.predict([target_seq] + states_value)

                sampled_token_index = np.argmax(output_tokens[0, -1, :])
                sampled_char = self.output_tokens[sampled_token_index]
                decoded_sentence += sampled_char

                if (sampled_char == '\n' or
                        len(decoded_sentence) > self.max_decoder_seq_length):
                    stop_condition = True

                target_seq = np.zeros((1, 1, self.num_decoder_tokens))
                target_seq[0, 0, sampled_token_index] = 1.

                states_value = [h, c]

            return decoded_sentence

    def translate(self, sentences):
        input_vec = self.fit_data(sentences)

        decoded_sequences = []
        for seq_idx in range(input_vec.shape[0]):
            decoded_sequences.append(self.decode_sequence(input_vec[seq_idx: seq_idx+1]))

        return decoded_sequences
