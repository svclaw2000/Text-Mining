import re
import pandas as pd
from utils.config_handler import ConfigParser
from preprocessing.word_extractor import Extractor
import pickle

def clean_sentence(s, str_type=None):
    if str_type == 'modern':
        s = re.sub('[(].*?[)]', '', s)
        s = re.sub('\t', '', s)
    elif str_type == 'sijo':
        error = re.findall('[\u2E80-\u2FD5\u3190-\u319f\u3400-\u4DBF\u4E00-\u9FCC\uF900-\uFAAD]+^[(]', s)
        if error:
            print('[ERROR] No explanation of %s' %error)
        s = re.sub('[\u2E80-\u2FD5\u3190-\u319f\u3400-\u4DBF\u4E00-\u9FCC\uF900-\uFAAD()\tㅣ]', '', s)
        s = re.sub('[\U00020000-\U0002A6DF]', '', s)
        s = re.sub('[\U0002A700–\U0002B81F]', '', s)
        s = re.sub('[\U0002B820–\U0002CEAF]', '', s)
    s = re.sub('[!.,?]', '', s)
    s = re.sub(' +', ' ', s)
    return '\n'.join([_s.strip() for _s in s[:-1].split('\n')])

def run():
    config = ConfigParser()
    config.load_from_file('config/preprocessing.conf')
    f_names = config['categories']

    print('[INFO] Start preprocess %s' %f_names)

    total_data = []
    each_lines = []

    for f_name in f_names:
        f_sijo = open('data/%s' %f_name, 'r', encoding='utf-8')
        f_modern = open('data/%s_modern' %f_name, 'r', encoding='utf-8')

        sijo = ''
        modern = ''

        while True:
            line_sijo = f_sijo.readline()
            line_modern = f_modern.readline()

            if not line_sijo and not line_modern:
                if sijo and modern:
                    total_data.append([clean_sentence(sijo, 'sijo'), clean_sentence(modern, 'modern'), f_name])
                break
            elif not line_sijo or not line_modern:
                print("Line doesn't match")
                break

            if line_sijo == '\n' and line_modern == '\n':
                total_data.append([clean_sentence(sijo, 'sijo'), clean_sentence(modern, 'modern'), f_name])
                sijo = ''
                modern = ''
                continue
            elif line_sijo == '\n' or line_modern == '\n':
                print("Line doesn't match")
                break

            each_lines.append([clean_sentence(line_sijo.strip(' \t'), 'sijo'), clean_sentence(line_modern.strip(' \t'), 'modern'), f_name])
            sijo += line_sijo.strip(' \t')
            modern += line_modern.strip(' \t')

        f_sijo.close()
        f_modern.close()

    print('[INFO] End preprocess %s sijos' %len(total_data))
    final = pd.DataFrame(total_data, columns=['sijo', 'modern', 'category'])
    final_lines = pd.DataFrame(each_lines, columns=['sijo', 'modern', 'category'])
    extractor = Extractor()
    final['keywords'] = extractor.extract_keyword(final['modern'])
    final_lines['keywords'] = extractor.extract_keyword(final_lines['modern'])
    final.to_csv('%s.csv' %config['output_path'], index=False)
    final_lines.to_csv('%s_lines.csv' %config['output_path'], index=False)
    pickle.dump(final, open('%s.pkl' %config['output_path'], 'wb'))
    pickle.dump(final, open('%s_lines.pkl' %config['output_path'], 'wb'))
    print('[INFO] Saved data at %s.' %config['output_path'])

