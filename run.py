from run_model import Generate_poem
from translation_model.Seq2Seq import S2S_Model
import sys

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print('실행방법: python3 run.py train|generate')
        exit()

    mode =  sys.argv[1]
    if mode not in ('train', 'generate'):
        print('실행방법: python3 run.py train|generate')
        exit()

    if mode == 'train':
        print('모델 학습')
        print('')

        print('시 생성 모델의 파라미터')
        print('1. 데이터 파일 위치')
        gen_data_path = input('(기본값: data/total_data.csv) ')
        gen_data_path = 'data/total_data.csv' if not gen_data_path else gen_data_path
        print('2. epoch 수')
        gen_epochs = input('(기본값: 250) ')
        gen_epochs = 250 if not gen_epochs else int(gen_epochs)
        print('3. batch_size')
        gen_batch_size = input('(기본값: 1000) ')
        gen_batch_size = 1000 if not gen_batch_size else int(gen_batch_size)
        print()

        print('시조 번역 모델의 파라미터')
        print('1. 사용할 데이터의 최대 길이')
        max_length = input('(기본값: 60) ')
        max_length = 60 if not max_length else int(max_length)
        print('2. 임베딩 층 사용 여부')
        embedding = input('(기본값: 0) ')
        embedding = False if embedding == '0' or not embedding else True
        print('3. 입력값의 토큰화 정도 0:글자 1:공백 2:토큰')
        input_tokenize = input('(기본값: 0) ')
        input_tokenize = 0 if not input_tokenize else int(input_tokenize)
        print('4. 출력값의 토큰화 정도 0:글자 1:공백 2:토큰')
        output_tokenize = input('(기본값: 0) ')
        output_tokenize = 0 if not output_tokenize else int(output_tokenize)
        print('5. 입력값 Word2Vec 사용 여부 0:사용안함 1:Pre-Trained 2:Train')
        input_gensim = input('(기본값: 0) ')
        input_gensim = 0 if not input_gensim else int(input_gensim)
        print('6. 출력값 Word2Vec 사용 여부 0:사용안함 1:Pre-Trained 2:Train')
        output_gensim = input('(기본값: 0) ')
        output_gensim = 0 if not output_gensim else int(output_gensim)
        print('7. epoch 수')
        epochs = input('(기본값: 300) ')
        epochs = 300 if not epochs else int(epochs)
        print('8. batch_size')
        batch_size = input('(기본값: 600) ')
        batch_size = 600 if not batch_size else int(batch_size)

        print('학습 시작')
        gen_model = Generate_poem()
        gen_model.data_preprocess(gen_data_path)
        gen_model.create_model(epochs=gen_epochs, batch_size=gen_batch_size)

        s2s_model = S2S_Model(
            max_length=max_length,
            embedding=embedding,
            input_tokenize=input_tokenize,
            output_tokenize=output_tokenize,
            input_gensim=input_gensim,
            output_gensim=output_gensim,
            epochs=epochs,
            batch_size=batch_size
        )

    elif mode == 'generate':
        print('시조 생성')
        print('')

        print('데이터 파일 위치')
        gen_data_path = input('(기본값: data/total_data.csv) ')
        gen_data_path = 'data/total_data.csv' if not gen_data_path else gen_data_path

        gen_h5 = input('생성 모델 경로: ')
        trans_h5 = input('번역 모델 경로: ')

        if not gen_h5 or not trans_h5:
            print('모델 경로 확인 후 다시 시도해주세요.')
            exit()

        init_word = input('소재: ')
        if not init_word:
            print('소재를 입력해주세요.')
            exit()
        poem_length = input('시의 길이: ')
        if not poem_length:
            print('시의 길이를 입력해주세요.')
            exit()
        poem_length = int(poem_length)

        gen_model = Generate_poem()
        gen_model.data_preprocess(gen_data_path)
        gen_model.create_model(loadpath='generate_model.h5')
        generated = gen_model.sentence_generation(init_word, poem_length)
        print('현대시: ', generated)

        s2s_model = S2S_Model(
            load_path=trans_h5
        )
        translated = s2s_model.translate([generated])
        print('시조', translated)