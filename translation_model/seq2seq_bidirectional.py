import numpy as np
import pickle

sijo = pickle.load(open('../data/preprocessed.pkl', 'rb'))

bind_texts = []

for row in sijo.itertuples():
    temp_input = row[2].split('\n')
    temp_target = row[1].split('\n')
    for _input, _target in zip(temp_input, temp_target):
        if len(_input) > 60:
            continue
        bind_texts.append((_input, '\t%s\n' %_target))

np.random.seed(1234)
np.random.shuffle(bind_texts)
test_size = int(len(bind_texts) * 0.1)
train_data, test_data = bind_texts[:-test_size], bind_texts[-test_size:]
input_texts, target_texts = [d[0] for d in train_data], [d[1] for d in train_data]
test_input_texts, test_target_texts = [d[0] for d in test_data], [d[1] for d in test_data]

latent_dim = 256
input_characters = set()
target_characters = set()
for input_text, target_text in zip(input_texts, target_texts):
    for ch in input_text:
        if ch not in input_characters:
            input_characters.add(ch)
    for ch in target_text:
        if ch not in target_characters:
            target_characters.add(ch)

input_characters.add('<UNK>')

input_characters = sorted(list(input_characters))
input_characters.insert(0, '<PAD>')
target_characters = sorted(list(target_characters))
target_characters[0], target_characters[1] = target_characters[1], target_characters[0]
num_encoder_tokens = len(input_characters)
num_decoder_tokens = len(target_characters)
max_encoder_seq_length = max([len(txt) for txt in input_texts])
max_decoder_seq_length = max([len(txt) for txt in target_texts])

input_token_index = { char: id for id, char in enumerate(input_characters)}
target_token_index = { char: id for id, char in enumerate(target_characters)}
reverse_target_char_index = dict((i, char) for char, i in target_token_index.items())

encoder_input_data = np.zeros(shape=(len(input_texts), max_encoder_seq_length, num_encoder_tokens), dtype='float32')
decoder_input_data = np.zeros(shape=(len(target_texts), max_decoder_seq_length, num_decoder_tokens), dtype='float32')
decoder_target_data = np.zeros(shape=(len(target_texts), max_decoder_seq_length,
                                      num_decoder_tokens), dtype='float32')

for i, (input_text, target_text) in enumerate(zip(input_texts, target_texts)):
    for j, ch in enumerate(input_text):
        encoder_input_data[i, j, input_token_index[ch]] = 1.
    for j, ch in enumerate(target_text):
        decoder_input_data[i, j, target_token_index[ch]] = 1.
        if j > 0:
            decoder_target_data[i, j-1, target_token_index[ch]] = 1.

from keras.models import Model, load_model
from keras.layers import Input, LSTM, Dense, BatchNormalization, Dropout, Reshape, Bidirectional
from keras.callbacks import EarlyStopping

# a part of encoder

encoder_inputs = Input(shape=(None, num_encoder_tokens), name='encoder_input')
encoder = Bidirectional(LSTM(latent_dim, return_sequences=True, return_state=True, name='encoder'))
encoder_outputs, state_h_f, state_c_f, state_h_b, state_c_b = encoder(encoder_inputs)
encoder_states = [state_h_f, state_c_f, state_h_b, state_c_b]

# a part of decoder
decoder_inputs = Input(shape=(None, num_decoder_tokens), name='decoder_input')
decoder_lstm = Bidirectional(LSTM(latent_dim, return_sequences=True, return_state=True, name='decoder'))
decoder_outputs, _, _, _, _ = decoder_lstm(decoder_inputs, initial_state=encoder_states)
dropout = Dropout(0.2)
batchNorm = BatchNormalization()
decoder_dense = Dense(num_decoder_tokens, activation='softmax')
decoder_outputs = decoder_dense(batchNorm(dropout(decoder_outputs)))

# a model to train
model = Model([encoder_inputs, decoder_inputs], decoder_outputs)

batch_size=300
epochs=400
optimizer='adam'
loss='categorical_crossentropy'
load_model_path='s2s_failed.h5'
# load_model_path=None
save_model_path='s2s.h5'

if load_model_path is not None:
    model.load_weights(load_model_path)
else:
    model.compile(optimizer, loss)
    model.fit([encoder_input_data, decoder_input_data], decoder_target_data,
              batch_size=batch_size,
              epochs=epochs,
              validation_split=0.2,
              callbacks=[EarlyStopping(monitor='val_loss', patience=10, verbose=0, mode='auto')]
              )
    model.save(save_model_path)

# encoder model to decode
encoder_model = Model(encoder_inputs, encoder_states)

# decoder model to decode
decoder_state_input_h_f = Input(shape=(latent_dim,))
decoder_state_input_c_f = Input(shape=(latent_dim,))
decoder_state_input_h_b = Input(shape=(latent_dim,))
decoder_state_input_c_b = Input(shape=(latent_dim,))
decoder_states_inputs = [decoder_state_input_h_f, decoder_state_input_c_f, decoder_state_input_h_b, decoder_state_input_c_b]
decoder_outputs, state_h_f, state_c_f, state_h_b, state_c_b = decoder_lstm(decoder_inputs, initial_state=decoder_states_inputs)
decoder_states = [state_h_f, state_c_f, state_h_b, state_c_b]
decoder_outputs = decoder_dense(decoder_outputs)
decoder_model = Model(
    [decoder_inputs] + decoder_states_inputs,
    [decoder_outputs] + decoder_states)

def decode_sequence(input_seq):
    # Encode the input as state vectors. [state_h, state_c]
    states_value = encoder_model.predict(input_seq)

    # Generate empty target sequence of length 1.
    target_seq = np.zeros((1, 1, num_decoder_tokens))
    # Populate the first character of target sequence with the start character.
    target_seq[0, 0, target_token_index['\t']] = 1.

    # 점점 디코드된 문자열을 추가해나감.
    stop_condition = False
    decoded_sentence = ''
    while not stop_condition:
        output_tokens, h_f, c_f, h_b, c_b = decoder_model.predict([target_seq] + states_value)

        # Sample a token
        sampled_token_index = np.argmax(output_tokens[0, -1, :])
        sampled_char = reverse_target_char_index[sampled_token_index]
        decoded_sentence += sampled_char

        # Exit condition: either hit max length
        # or find stop character.
        if (sampled_char == '\n' or
                len(decoded_sentence) > max_decoder_seq_length):
            stop_condition = True

        # Update the target sequence (of length 1).
        target_seq = np.zeros((1, 1, num_decoder_tokens))
        target_seq[0, 0, sampled_token_index] = 1.

        # Update states
        states_value = [h_f, c_f, h_b, c_b]

    return decoded_sentence

def translate(input_seq):
    # 입력을 모델에 주입할 수 있도록 벡터화한다.
    input_vec = np.zeros(shape=(len(input_seq), max_encoder_seq_length, num_encoder_tokens), dtype='float32')
    for i, txt in enumerate(input_seq):
        for j, ch in enumerate(txt):
            input_vec[i, j, input_token_index[ch] if ch in input_token_index else input_token_index['<UNK>']] = 1.

    decoded_sequences = []
    for seq_idx in range(input_vec.shape[0]):
        decoded_sequences.append(decode_sequence(input_vec[seq_idx: seq_idx+1]))

    return decoded_sequences

start_idx = 10
end_idx = start_idx + 10

for _input, _target, _pred in zip(input_texts[start_idx:end_idx], target_texts[start_idx:end_idx], translate(input_texts[start_idx:end_idx])):
    print(_input)
    print(_target[1:-1])
    print(_pred)
    print()

start_idx = 40
end_idx = start_idx + 10

for _input, _target, _pred in zip(test_input_texts[start_idx:end_idx], test_target_texts[start_idx:end_idx], translate(test_input_texts[start_idx:end_idx])):
    print(_input)
    print(_target[1:-1])
    print(_pred)
    print()
