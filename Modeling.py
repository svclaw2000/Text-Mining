import numpy as np
from gensim.models import Word2Vec
import tensorflow as tf

get_ipython().system('pip install numpy --upgrade')


# 저장해놓은 word2vec 모델 불러오기 
path = "word2vec/all_sentences_for_word2vec.txt"

with open(path, "r", encoding='utf-8') as _file:
    text = _file.read().split()
    # self.text = self.text[:10000]

sijo2vec = Word2Vec.load("word2vec/all_word2vec_model")

vocab_inv = list(sijo2vec.wv.vocab.keys()) # key 값을 리스트화함. 
vocab = {x: i for i, x in enumerate(vocab_inv)} # 0,1,2 등 인덱스와 단어를 dict 로 매칭시켜놓음

X = text[:] # 텍스트 파일 전체를 복사 
y = [text[0]] + text[1:] 

# 개수 확인 

print(len(X))  # 5452 
print(len(vocab_inv))  # 1645


#### 모델에 적합한 데이터셋으로 변경 

class TextLoader:
    
    def __init__(self, path):
        
        self.text = tokenized_data

        self.sijo2vec = Word2Vec.load("word2vec/all_word2vec_model")
        
        self.vocab, self.words = self.build_vocab()

        self.X = self.text[:] # 텍스트 파일 전체를 복사 - self.text 와 같은 의미
        self.y = [self.text[0]] + self.text[1:] # 

    def build_vocab(self):
        vocab_inv = list(self.sijo2vec.wv.vocab.keys()) # key 값을 리스트화 / 글자만
        vocab = {x: i for i, x in enumerate(vocab_inv)} # 0,1,2 등 인덱스와 단어를 dict 로 매칭시켜놓음
        return vocab, vocab_inv

    def next_batch(self, batch_size, seq_length):
        start = np.random.randint(0, len(self.X)-batch_size*seq_length) # 랜덤으로 위치를 정함 - 끝의 값을 구하면 안됨 / 시작 위치를 글자를 다 배치사이즈와 시퀀스렝스로 구함 // 마지막까지는 안가겠다는 뜻
        end   = start + batch_size*seq_length # 몇 단어를 가져올지

        X_words = self.X[start:end] # 말그대로 글자
        y_words = self.y[start:end]
        print(X_words)
        print(y_words)
        
        X_idx = np.empty((batch_size, seq_length), dtype=np.int64) # 글자의 인덱스
        y_idx = np.empty((batch_size, seq_length), dtype=np.int64)
        X_wv = np.empty((batch_size, seq_length, 100)) # 글자의 word2vec 
        y_wv = np.empty((batch_size, seq_length, 100))
        # 위에서 만들어준 자리에 따라 (저장공간 설정하는 과정) 아래에서 for 문을 돌며 값을 가져옴 / 그냥 하면 안되는 이유 : append는 느림, numpy의 경우에는 1 2 3 4 붙어있어야 함 / 5를 넣는다 하면 이걸 어딘가 복사해서 5를 붙여야 함.
        
        print(X_idx.shape)
        
        for i in range(batch_size):
            for j in range(seq_length):
                X_idx[i, j] = self.vocab[X_words[i*seq_length+j]]
                y_idx[i, j] = self.vocab[y_words[i*seq_length+j]]

                X_wv[i, j] = self.sijo2vec.wv[X_words[i*seq_length+j]]
                y_wv[i, j] = self.sijo2vec.wv[y_words[i*seq_length+j]]

        return X_wv, X_idx, y_wv, y_idx

# 모델링 

num_layers  = 3
hidden_size = 512
batch_size  = 1
max_length  = 10
learning_rate = 0.001
 
# 저장해놓은 토큰화 데이터 불러오기 
old = np.load
np.load = lambda *a,**k: old(*a,allow_pickle=True)
tokenized_data = list(np.load("okt_tokenized_data_save.npy"))

loader = TextLoader("word2vec/all_sentences_for_word2vec.txt")
vocab_size = len(loader.vocab)


# x 변수 생성 
X = tf.placeholder(tf.float32, [None, None, 20])

cells = [tf.nn.rnn_cell.BasicLSTMCell(hidden_size) for _ in range(num_layers)]
cells = tf.nn.rnn_cell.MultiRNNCell(cells) # , state_is_tuple=True


initial_state = cells.zero_state(batch_size, tf.float32)
outputs, states = tf.nn.dynamic_rnn(cells, X,initial_state = initial_state, dtype=tf.float32)

outputs = tf.reshape(outputs, [-1, hidden_size])
logits = tf.compat.v1.layers.dense(outputs, vocab_size)

y_softmax = tf.nn.softmax(logits)

pred = tf.argmax(y_softmax, axis=1)
pred = tf.reshape(pred, [batch_size, -1])

# 입력값이 주어졌을 때 다음 값이 예측되도록 
sentence = ["자연"]
print(sentence)

with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())
    
    # [배치사이즈, max_length, 20]
    vec = np.empty((1, max_length, 20)) # 시작 글자를 주고 다음 단어를 예측 

    for i, word in enumerate(sentence):
        vec[:, i, :] = loader.sijo2vec.wv[word]
    
    # 매 이터레이션마다 글자 하나씩 생성
    state = sess.run(states, feed_dict={X: vec}) #입력단어 sentence 이후에 들어올 단어를 예측
    
    for i in range(15): 
        vec = loader.sijo2vec.wv[sentence[-1]].reshape(1, 1, 20)
        
        pred_char, state = sess.run([pred, states], feed_dict={X: vec, initial_state: state}) 
        # 원래 initial stete 는 0 이었으나
        # 이전 스텝에 갖고 있는 state 값을 다음 스텝에 넣어줌 // 입력단어들을 그 다음 스텝에 넣어주는 것임
        # pred_char, state = sess.run([pred, states], feed_dict={X: vec})

        pred_char = loader.words[pred_char[0][-1]]
        sentence.append(pred_char)

for i, word in enumerate(sentence):
    print(word, end=" ")
    if (i+1) % 5 == 0:
        print()
