from konlpy.tag import Komoran

class Extractor:
    def __init__(self):
        self.komoran = Komoran()

    def extract(self, texts, use_tags=None):
        tag_list = [self.komoran.pos(text, join=True) for text in texts]
        ret_tags = []
        if not use_tags:
            for tags in tag_list:
                ret_tag = []
                for tag in tags:
                    ret_tag.append(tag.split('/')[0])
                ret_tags.append(ret_tag)
            return ret_tags
        for tags in tag_list:
            ret_tag = []
            for tag in tags:
                for use_tag in use_tags:
                    if use_tag in tag:
                        ret_tag.append(tag.split('/')[0])
                        break
            ret_tags.append(ret_tag)
        return ret_tags