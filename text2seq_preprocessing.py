
# #### 패키지 import

import pandas as pd
import numpy as np

from keras.utils import to_categorical

from keras_preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences


# #### data 불러오기 

sijo = pd.read_csv('sijo.csv',encoding = 'utf-8')

sijo.modern = [s.replace(".",". ") for s in sijo.modern]
sijo.modern = [s.replace("?","? ") for s in sijo.modern]
sijo.modern = [s.replace("!","! ") for s in sijo.modern]
sijo.modern = [s.replace(",",", ") for s in sijo.modern]

sijo.modern = [s.replace("   "," ") for s in sijo.modern]
sijo.modern = [s.replace("  "," ") for s in sijo.modern]

sijo.modern = [s.replace("[^ㄱ-ㅎㅏ-ㅣ가-힣 ]","") for s in sijo.modern]

print(sijo.modern)


data_peonsijo = open("data/peongsijo_modern", encoding = 'utf-8-sig')
data_peonsijo2 = open("data/peongsijo2_modern", encoding = 'utf-8-sig')
data_yeonsijo = open("data/yeonsijo_modern", encoding = 'utf-8-sig')
data_yongbieocheonga = open("data/yongbieocheonga_modern", encoding = 'utf-8-sig')

data_peonsijo = data_peonsijo.read()
data_peonsijo2 = data_peonsijo2.read()
data_yeonsijo = data_yeonsijo.read()
data_yongbieocheonga = data_yongbieocheonga.read()

data = data_peonsijo+'\n'+data_peonsijo2+'\n'+data_yeonsijo+'\n'+data_yongbieocheonga

sample_list = [v for v in data.split('\n') if v]

print('가장 긴 시의 길이: {}'.format(len(max(sample_list))))
print('총 샘플의 개수 : {}'.format(len(sample_list))) # 현재 샘플의 개수

sequences = list()

t = Tokenizer()
t.fit_on_texts(sample_list)
vocab_size = len(t.word_index)+1

for text in list(sample_list):
    print(text)
    for line in text.split('\n'): # \n을 기준으로 문장 토큰화
        encoded = t.texts_to_sequences([line])[0]  # 텍스트를 숫자 시퀀스로 변경 
        for i in range(1, len(encoded)):
            sequence = encoded[:i+1]
            sequences.append(sequence)


print('훈련 데이터의 개수: %d' % len(sequences)) 
print(max(len(l) for l in sequences))   # 65

# 문자 길이 맞춰주기 
sequences = pad_sequences(sequences, maxlen=65, padding='pre')  
sequences = np.array(sequences)

# 모델링하기 위해 데이터 저장 
np.save("sequences_save",sequences)


