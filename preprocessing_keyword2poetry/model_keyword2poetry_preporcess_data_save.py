# 패키지 import 
import pandas as pd
from konlpy.tag import Okt
from konlpy.tag import Kkma 
import urllib
import json
from bs4 import BeautifulSoup

from tensorflow.keras.utils import to_categorical
import re
import numpy as np
import os
os.chdir(r"D:\git\Text-Mining")

# 데이터 전처리 
def reprocessing(data):

    data = [s.replace("."," ") for s in data]
    data = [s.replace("?"," ") for s in data]
    data = [s.replace("!"," ") for s in data]
    data = [s.replace(","," ") for s in data]
    data = [s.replace(","," ") for s in data]
    data = [s.replace("·"," ") for s in data]

    data = [s.replace("  "," ") for s in data]
    data = [re.sub(r'\([^-)]*\)','',s) for s in data]

    data = [s.replace("[^ㄱ-ㅎㅏ-ㅣ가-힣 ]","") for s in data]
    return data

# 맞춤법 검사 
def spellchecker(q):
    
    if len(q)<500:
        params = urllib.parse.urlencode({
            "_callback": "",
            "q": q
        })
        
        # 네이버 맞춤법 검사기 사용하여 문법 교정 
        data = urllib.request.urlopen("https://m.search.naver.com/p/csearch/ocontent/spellchecker.nhn?" + params)
        data = data.read().decode("utf-8")[1:-2]
        data = json.loads(data)
        data = data["message"]["result"]["html"]
        data =  BeautifulSoup(data, "html.parser").getText()
        
    else:
        data = ''
    
    return data

data_peonsijo = open("data/peongsijo_modern", encoding = 'utf-8-sig')
data_peonsijo2 = open("data/peongsijo2_modern", encoding = 'utf-8-sig')
data_yeonsijo = open("data/yeonsijo_modern", encoding = 'utf-8-sig')
data_yongbieocheonga = open("data/yongbieocheonga_modern", encoding = 'utf-8-sig')

data_peonsijo = data_peonsijo.read()
data_peonsijo2 = data_peonsijo2.read()
data_yeonsijo = data_yeonsijo.read()
data_yongbieocheonga = data_yongbieocheonga.read()

data = data_peonsijo+'\n'+data_peonsijo2+'\n'+data_yeonsijo+'\n'+data_yongbieocheonga

sample_list = [v for v in data.split('\n') if v]

poem_data = pd.read_csv("data/modern_poem_collection.csv")
poem_data.head()

poem_list = []
poem_list.extend(poem_data["0"].values)
poem_list

total_data_list = sample_list + poem_list

total_data_list = reprocessing(total_data_list)
total_data_list = [spellchecker(data) for data in total_data_list]

total_data_list = [_char for _char in total_data_list if _char]
total_data_list = list(set(total_data_list))

pd.DataFrame(total_data_list, columns = ["modern"]).to_csv("data/total_data.csv")
