from konlpy.tag import Komoran
from textrank import KeywordSummarizer
import pandas as pd
from utils.config_handler import ConfigParser
import numpy as np
from tqdm import tqdm


class Extractor:
    def __init__(self):
        self.komoran = Komoran()
        self.config = ConfigParser()
        self.config.load_from_file('config/preprocessing.conf')
        self.min_word_size = self.config['min_word_size']
        # self.keyword_extractor = KeywordSummarizer(
        #     tokenize=self.tokenize_noun,
        #     window=1,
        #     verbose=False
        # )

    def tokenize_noun(self, sent):
        try:
            words = self.komoran.pos(sent, join=True)
            ret = [w for w in words if '/NN' in w and len(w.split('/NN')[0]) >= self.min_word_size]
            return ret
        except:
            return []

    def extract_keyword(self, sents):
        return [self.tokenize_noun(s) if s is not np.nan else [] for s in tqdm(sents)]
